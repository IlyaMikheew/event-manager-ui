import React, { useEffect, useState } from "react";
import { Switch, Route } from "react-router-dom";
import styled from "styled-components";
import Cookies from "universal-cookie";
import { HomePage } from "../home-page";
import LoginPageContainer from "../../modules/login-page/containers/login-page-container";
import RegistrationPageContainer from "../../modules/registration-page/containers/registration-page-container";
import EventPageContainer from "../../modules/event-page/containers/event-page-container";
import UserPageContainer from "../../modules/user-page/containers/user-page-container";
import { connect } from "../../services/web-socket-service";

const StyledApp = styled.div`
  width: 100%;
  height: 100%;
  background-color: #fafafa;
  flex-direction: column;
  display: flex;
  min-width: 700px;
`;

const cookies = new Cookies();

const App = () => {
  const [userId, setUserId] = useState(null);

  useEffect(() => {
    if (cookies.get("token")) {
      setUserId(cookies.get("userId"));
    }
  }, []);

  useEffect(() => {
    if (userId) {
      connect();
    }
  }, [userId]);

  return (
    <StyledApp>
      <Switch>
        <Route
          path="/registration"
          component={RegistrationPageContainer}
          exact
        />
        <Route path="/event/:eventId" component={EventPageContainer} exact />
        <Route path="/user/:userId" component={UserPageContainer} exact />
        <Route path="/login" component={LoginPageContainer} exact />
        <Route path="/home" component={HomePage} exact />
        <Route
          path="/"
          component={
            cookies.get("userId") === undefined ? LoginPageContainer : HomePage
          }
        />
      </Switch>
    </StyledApp>
  );
};

export default App;
