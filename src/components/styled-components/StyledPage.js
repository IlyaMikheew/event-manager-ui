import styled from "styled-components";

const StyledPage = styled.div`
  width: 80%;
  height: 100%;
  margin-left: 10%;
  background-color: white;
  min-width: 700px;
`;

export default StyledPage;
