import React from "react";
import styled from "styled-components";
import { Image } from "antd";
import { Link, useHistory } from "react-router-dom";
import { HeartFilled, HeartOutlined, SelectOutlined } from "@ant-design/icons";

const Panel = styled.div`
  width: 300px;
  height: 500px;
  background-color: white;
  border: 1px solid #D9D9D9;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  inset 0px 4px 4px rgba(0, 0, 0, 0.25);
  display: flex;
  flex-direction: column;
  position: absolute;
  top: 20%;
  left: 15%;
  padding: 10px;
`;

const ImageWrapper = styled.div`
  flex: 1;
`;

const StyledAction = styled.div`
  color: #1890ff;
  font-size: 16px;
  cursor: pointer;
  margin-left: 5px;
`;

const EventInfoWrapper = styled.div`
  flex: 1;
  font-weight: 300;
  font-size: 15px;
  overflow: auto;
`;

const StyledHeartOutlined = styled(HeartOutlined)`
  margin-right: 10px;
`;

const StyledHeartFilled = styled(HeartFilled)`
  margin-right: 10px;
  color: red;
`;

const StyledSelectOutlined = styled(SelectOutlined)`
  margin-right: 10px;
`;

const StyledEventName = styled.span`
  font-size: 17px;
  margin-left: 5px;
`;

const EventPanel = ({ event, subscribe, unsubscribe, isSubscribed }) => {
  const history = useHistory();

  return (
    <Panel>
      <ImageWrapper>
        <Image
          src={`data:image/png;base64,${event.image}`}
          height={230}
          width={280}
        />
      </ImageWrapper>
      <EventInfoWrapper>
        <Link to={`/event/${event.id}`}>
          <StyledEventName>{event.name}</StyledEventName>
        </Link>
        {isSubscribed ? (
          <StyledAction
            onClick={() => {
              history.push(`/home?eventId=${event.id}`);
              unsubscribe(event.id);
            }}
          >
            <StyledHeartFilled />
            Unsubscribe
          </StyledAction>
        ) : (
          <StyledAction
            onClick={() => {
              history.push(`/home?eventId=${event.id}`);
              subscribe(event.id);
            }}
          >
            <StyledHeartOutlined />
            Subscribe
          </StyledAction>
        )}
        <Link to={`/event/${event.id}`}>
          <StyledAction>
            <StyledSelectOutlined />
            Open event page
          </StyledAction>
        </Link>
        <hr color="#D9D9D9" width="98%" size={1} />
        <span>
          <b>Date/Time:</b> {event.startDate.replace("T", " ")}
        </span>
        <br />
        <span>
          <b>Address:</b> {event.location.address}
        </span>
        <br />
        <span>
          <b>Description:</b> <br />
          {event.description}
        </span>
      </EventInfoWrapper>
    </Panel>
  );
};
export default EventPanel;
