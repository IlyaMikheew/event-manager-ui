import React, { Component } from "react";
import { Button, Result } from "antd";

export default class ErrorBoundary extends Component {
  state = {
    hasError: false,
  };

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      return (
        <Result
          status="500"
          title="500"
          subTitle="Sorry, something went wrong."
          extra={
            <a href="/home">
              <Button type="primary">Back Home</Button>
            </a>
          }
        />
      );
    }
    return this.props.children;
  }
}
