import React from "react";
import { Tabs } from "antd";
import styled from "styled-components";
import { useLocation } from "react-router-dom";
import "antd/dist/antd.css";
import EventListContainer from "../../modules/events-list/containers/event-list-container";
import MainMapContainer from "../../modules/main-map/containers/main-map-container";
import HeaderContainer from "../../modules/header/containers/header-container";

const { TabPane } = Tabs;

const StyledTabs = styled(Tabs)`
  margin: 0 5%;
`;

const StyledHomePage = styled.div`
  width: 80%;
  height: 100%;
  margin-left: 10%;
  background-color: white;
  min-width: 700px;
`;

const StyledTabPane = styled(TabPane)``;

const HomePage = () => {
  const location = useLocation();
  const eventId = new URLSearchParams(location.search).get("eventId");
  const props =
    location.search === "" ? {} : { selectedEventId: Number(eventId) };

  return (
    <>
      <HeaderContainer />
      <StyledHomePage>
        <StyledTabs
          defaultActiveKey={location.search === "" ? "1" : "2"}
          centered={false}
          size="large"
        >
          <StyledTabPane tab="LIST" key="1">
            <EventListContainer />
          </StyledTabPane>
          <TabPane tab="MAP" key="2">
            <MainMapContainer {...props} />
          </TabPane>
        </StyledTabs>
      </StyledHomePage>
    </>
  );
};

export default HomePage;
