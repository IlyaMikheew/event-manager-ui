import React from "react";
import { Form, Button, Input } from "antd";
import styled from "styled-components";
import Cookies from "universal-cookie";

const { TextArea } = Input;

const StyledForm = styled(Form)`
  margin-top: 10px;
`;

const cookies = new Cookies();

const CommentEditor = ({ onSubmit, eventId }) => {
  const [form] = Form.useForm();

  const setComment = (values) => {
    if (values.text && values.text !== "") {
      onSubmit({
        eventId,
        userId: Number(cookies.get("userId")),
        text: values.text,
        likes: 0,
        dislikes: 0,
      });
      form.resetFields();
    }
  };

  return (
    <StyledForm onFinish={setComment} form={form}>
      <Form.Item name="text">
        <TextArea rows={4} maxLength={250} />
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" type="primary">
          Add Comment
        </Button>
      </Form.Item>
    </StyledForm>
  );
};

export default CommentEditor;
