import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import createSagaMiddleware from "redux-saga";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import { YMaps } from "react-yandex-maps";
import { rootWatcher } from "./modules/sagas";
import rootReducer from "./modules/reducers";
import App from "./components/app/app";
import ErrorBoundary from "./components/error-boundary";

const saga = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(saga));
saga.run(rootWatcher);

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundary>
      <Router>
        <YMaps
          query={{
            apikey: "e3e6a6fc-03ae-4b67-bd2e-936d87065b94",
            lang: "ru_RU",
          }}
        >
          <App />
        </YMaps>
      </Router>
    </ErrorBoundary>
  </Provider>,
  document.getElementById("root")
);
