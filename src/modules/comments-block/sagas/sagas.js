import { put, call, takeEvery } from "redux-saga/effects";
import EMService from "../../../services/EMService";
import {
  commentsError,
  commentsLoaded,
  deleteLikeSuccess,
  setCommentSuccess,
  setLikeSuccess,
} from "../actions";

function* fetchCommentsWorker({ eventId, pagination }) {
  const service = new EMService();
  try {
    const data = yield call(service.getComments, { eventId, pagination });
    yield put(commentsLoaded(data.data));
  } catch (e) {
    yield put(commentsError(e));
  }
}

function* setCommentWorker({ comment }) {
  const service = new EMService();
  try {
    const data = yield call(service.setComment, comment);
    yield put(setCommentSuccess(data.data));
  } catch (e) {
    yield put(commentsError(e));
  }
}

function* setLikeWorker({ commentId, page }) {
  const service = new EMService();
  try {
    const data = yield call(service.setLikeOnComment, { commentId, page });
    yield put(setLikeSuccess(data.data));
  } catch (e) {
    yield put(commentsError(e));
  }
}

function* deleteLikeWorker({ commentId, page }) {
  const service = new EMService();
  try {
    const data = yield call(service.deleteLikeOnComment, { commentId, page });
    yield put(deleteLikeSuccess(data.data));
  } catch (e) {
    yield put(commentsError(e));
  }
}

export function* fetchCommentsWatcher() {
  yield takeEvery("FETCH_COMMENTS_REQUEST", fetchCommentsWorker);
}

export function* setCommentWatcher() {
  yield takeEvery("SET_COMMENT_REQUEST", setCommentWorker);
}

export function* setLikeWatcher() {
  yield takeEvery("SET_LIKE_REQUEST", setLikeWorker);
}

export function* deleteLikeWatcher() {
  yield takeEvery("DELETE_LIKE_REQUEST", deleteLikeWorker);
}
