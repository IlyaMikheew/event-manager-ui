import { connect } from "react-redux";
import {
  commentsRequested,
  deleteLikeRequest,
  setCommentRequest,
  setLikeRequest,
} from "../actions";
import CommentsBlock from "../components/comments-block";

const mapStateToProps = (state) => ({
  comments: state.commentsBlock.comments,
  total: state.commentsBlock.total,
  pagination: state.commentsBlock.pagination,
  likedComments: state.commentsBlock.likedComments,
});

const mapDispatchToProps = (dispatch) => ({
  getComments: (eventId, pagination) => {
    dispatch(commentsRequested(eventId, pagination));
  },
  setComment: (comment) => {
    dispatch(setCommentRequest(comment));
  },
  setLike: (commentId, page) => {
    dispatch(setLikeRequest(commentId, page));
  },
  deleteLike: (commentId, page) => {
    dispatch(deleteLikeRequest(commentId, page));
  },
});

const CommentsBlockContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentsBlock);

export default CommentsBlockContainer;
