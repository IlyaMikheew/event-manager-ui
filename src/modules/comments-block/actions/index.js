const commentsRequested = (eventId, pagination) => ({
  type: "FETCH_COMMENTS_REQUEST",
  eventId,
  pagination,
});

const commentsLoaded = (comments) => ({
  type: "FETCH_COMMENTS_SUCCESS",
  comments,
});

const commentsError = (error) => ({
  type: "FETCH_COMMENTS_FAILURE",
  error,
});

const setCommentRequest = (comment) => ({
  type: "SET_COMMENT_REQUEST",
  comment,
});

const setCommentSuccess = (comments) => ({
  type: "SET_COMMENT_SUCCESS",
  comments,
});

const setLikeRequest = (commentId, page) => ({
  type: "SET_LIKE_REQUEST",
  commentId,
  page,
});

const setLikeSuccess = (comments) => ({
  type: "SET_LIKE_SUCCESS",
  comments,
});

const deleteLikeRequest = (commentId, page) => ({
  type: "DELETE_LIKE_REQUEST",
  commentId,
  page,
});

const deleteLikeSuccess = (comments) => ({
  type: "DELETE_LIKE_SUCCESS",
  comments,
});

export {
  commentsRequested,
  commentsLoaded,
  commentsError,
  setCommentRequest,
  setCommentSuccess,
  setLikeRequest,
  setLikeSuccess,
  deleteLikeRequest,
  deleteLikeSuccess,
};
