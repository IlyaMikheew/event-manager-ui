import React, { createElement, useEffect } from "react";
import { List, Comment, Tooltip } from "antd";
import "antd/dist/antd.css";
import {
  // DislikeFilled,
  // DislikeOutlined,
  LikeFilled,
  LikeOutlined,
} from "@ant-design/icons";
import styled from "styled-components";
import CommentEditor from "../../../components/comment-editor/comment-editor";

const StyledLikeCount = styled.span`
  padding-left: 6px;
`;

const CommentsBlock = ({
  comments,
  eventId,
  pagination,
  total,
  getComments,
  setComment,
  setLike,
  deleteLike,
  likedComments,
}) => {
  useEffect(() => {
    getComments(eventId, pagination);
  }, [eventId, getComments, pagination]);

  const commentsData = comments.map((i) => ({
    actions: [
      <Tooltip key="comment-basic-like" title="Like">
        <span
          onClick={() =>
            likedComments.includes(i.id)
              ? deleteLike(i.id, pagination.page)
              : setLike(i.id, pagination.page)
          }
        >
          {createElement(
            likedComments.includes(i.id) ? LikeFilled : LikeOutlined
          )}
          <StyledLikeCount>{i.likes}</StyledLikeCount>
        </span>
      </Tooltip>,
    ],
    author: `${i.fname} ${i.lname}`,
    content: <p>{i.text}</p>,
    avatar: i.avatar
      ? `data:image/png;base64,${i.avatar}`
      : "https://windows10free.ru/uploads/posts/2017-04/1493287748_1487679899_icon-user-640x640.png",
    datetime: <span>{new Date(i.addTime).toDateString()}</span>,
  }));

  const listPagination = {
    size: "small",
    total,
    pageSize: pagination.limit,
    onChange: (page) =>
      getComments(eventId, {
        start: page * pagination.limit,
        limit: pagination.limit,
        page,
      }),
  };

  return (
    <>
      <List
        dataSource={commentsData}
        size="small"
        header="Comments"
        itemLayout="horizontal"
        renderItem={(props) => <Comment {...props} />}
        pagination={listPagination}
      />
      <CommentEditor onSubmit={setComment} eventId={eventId} />
    </>
  );
};
export default CommentsBlock;
