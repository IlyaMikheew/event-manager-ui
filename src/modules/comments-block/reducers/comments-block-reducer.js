const initialState = {
  comments: [],
  total: 0,
  pagination: {
    start: 0,
    limit: 2,
    page: 1,
  },
  likedComments: [],
  error: null,
};

const CommentsBlockReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_COMMENTS_REQUEST":
      return {
        ...state,
        pagination: action.pagination,
      };
    case "FETCH_COMMENTS_SUCCESS":
      return {
        ...state,
        comments: action.comments.page.content,
        total: action.comments.page.totalElements,
        likedComments: action.comments.likedComments,
      };
    case "FETCH_COMMENTS_ERROR":
      return {
        ...state,
        error: action.error,
      };
    case "SET_COMMENT_REQUEST":
      return {
        ...state,
      };
    case "SET_COMMENT_SUCCESS":
      return {
        ...state,
        comments: action.comments.content,
        total: action.comments.totalElements,
      };
    case "SET_LIKE_REQUEST":
      return {
        ...state,
      };
    case "SET_LIKE_SUCCESS":
      return {
        ...state,
        comments: action.comments.page.content,
        likedComments: action.comments.likedComments,
      };
    case "DELETE_LIKE_REQUEST":
      return {
        ...state,
      };
    case "DELETE_LIKE_SUCCESS":
      return {
        ...state,
        comments: action.comments.page.content,
        likedComments: action.comments.likedComments,
      };
    default:
      return state;
  }
};

export default CommentsBlockReducer;
