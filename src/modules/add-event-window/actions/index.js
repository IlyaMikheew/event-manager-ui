const createEventRequest = (event) => ({
  type: "CREATE_EVENT_REQUEST",
  event,
});

const createEventSuccess = () => ({
  type: "CREATE_EVENT_SUCCESS",
});

const createEventError = (error) => ({
  type: "CREATE_EVENT_ERROR",
  error,
});

export { createEventSuccess, createEventRequest, createEventError };
