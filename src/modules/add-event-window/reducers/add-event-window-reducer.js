const initialState = {
  error: null,
};

const AddEventWindowReducer = (state = initialState, action) => {
  switch (action.type) {
    case "CREATE_EVENT_REQUEST":
      return state;
    case "CREATE_EVENT_SUCCESS":
      return state;
    case "CREATE_EVENT_ERROR":
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};

export default AddEventWindowReducer;
