import React, { useEffect, useState } from "react";
import {
  Form,
  Input,
  Button,
  TimePicker,
  DatePicker,
  Space,
  Upload,
  Modal,
  Row,
  notification,
} from "antd";
import {
  PlusOutlined,
  MinusCircleOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import "antd/dist/antd.css";
import styled from "styled-components";
import { Map } from "react-yandex-maps";

const StyledForm = styled(Form)`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding-right: 30px;
  min-width: 500px;
`;

const StyledTextArea = styled(Input.TextArea)`
  width: 400px;
`;

const StyledDatePicker = styled(DatePicker)`
  margin-left: 20px;
`;

const StyledEndDatePicker = styled(DatePicker)`
  margin-left: 25px;
`;

const StyledTimePicker = styled(TimePicker)`
  margin-left: 6px;
`;

const DateTimeWrapper = styled(Form.Item)`
  display: flex;
  flex-direction: row;
`;

const StyledNameInput = styled(Input)`
  margin-left: 32px;
  width: 300px;
`;

const StyledLocationInput = styled.input`
  margin-left: 32px;
  width: 400px;
  height: 32px;
  margin-bottom: 30px;
`;

const StyledLocationWrapper = styled.div`
  display: flex;
  margin-left: 5px;
`;

const StyledUpload = styled(Upload)`
  margin-left: 32px;
`;

const geocode = async (ymaps, address) => {
  const response = await ymaps.geocode(address);
  const coordinates = await response.geoObjects
    .get(0)
    .geometry.getCoordinates();
  return coordinates;
};

const AddEventWindow = ({ createEvent, isVisible, setVisible }) => {
  const [form] = Form.useForm();

  useEffect(() => {
    form.resetFields();
  }, [form]);

  const [ymaps, setYMaps] = useState(null);
  return (
    <>
      <Modal
        width={700}
        title="Create Event"
        visible={isVisible}
        onOk={() => {
          form.submit();
        }}
        onCancel={() => {
          form.resetFields();
          setVisible(false);
        }}
      >
        <StyledForm
          form={form}
          onFinish={async () => {
            const event = form.getFieldsValue();
            const location = {};
            if (document.getElementById("location").value.length === 0) {
              notification.open({
                message: "Error",
                description: "Unable create event without location",
                duration: 5,
                type: "error",
              });
              return;
            }
            location.address = document.getElementById("location").value;
            location.coordinates = await geocode(ymaps, location.address);
            createEvent({
              ...event,
              location,
            });
            form.resetFields();
            setVisible(false);
          }}
        >
          <Form.Item
            name="image"
            label="Image"
            rules={[
              {
                required: true,
                message: "required",
              },
            ]}
          >
            <StyledUpload
              method="GET"
              listType="picture"
              maxCount={1}
              accept={"image/*"}
            >
              <Button icon={<UploadOutlined />}>Upload image</Button>
            </StyledUpload>
          </Form.Item>
          <Form.Item
            name="name"
            label="Name"
            rules={[
              {
                required: true,
                type: "string",
                max: 60,
                message: "",
              },
            ]}
          >
            <StyledNameInput />
          </Form.Item>
          <Form.Item
            name="description"
            label="Description"
            rules={[
              {
                required: true,
                type: "string",
                message: "",
              },
            ]}
          >
            <StyledTextArea rows={5} />
          </Form.Item>
          <Form.Item label="Start date">
            <Row>
              <DateTimeWrapper
                name="startDate"
                rules={[
                  {
                    required: true,
                    message: "",
                  },
                ]}
              >
                <StyledDatePicker />
              </DateTimeWrapper>
              <DateTimeWrapper
                name="startTime"
                rules={[
                  {
                    required: true,
                    message: "",
                  },
                ]}
              >
                <StyledTimePicker />
              </DateTimeWrapper>
            </Row>
          </Form.Item>
          <Form.Item label="End date">
            <Row>
              <DateTimeWrapper
                name="endDate"
                rules={[
                  {
                    required: true,
                    message: "",
                  },
                ]}
              >
                <StyledEndDatePicker />
              </DateTimeWrapper>
              <DateTimeWrapper
                name="endTime"
                rules={[
                  {
                    required: true,
                    message: "",
                  },
                ]}
              >
                <StyledTimePicker />
              </DateTimeWrapper>
            </Row>
          </Form.Item>
          <StyledLocationWrapper>
            <p>Location: </p>
            <StyledLocationInput
              className="ant-input"
              type="text"
              id="location"
            />
          </StyledLocationWrapper>
          <Form.List name="timelineItems">
            {(fields, { add, remove }) => (
              <>
                {fields.map(({ key, name, fieldKey, ...restField }) => (
                  <Space key={key} style={{ display: "flex" }} align="baseline">
                    <Form.Item
                      {...restField}
                      name={[name, "desc"]}
                      fieldKey={[fieldKey, "desc"]}
                      rules={[
                        {
                          required: true,
                          type: "string",
                          max: 200,
                          message: "",
                        },
                      ]}
                    >
                      <Input style={{ width: "300px" }} />
                    </Form.Item>
                    <Form.Item
                      {...restField}
                      name={[name, "time"]}
                      fieldKey={[fieldKey, "time"]}
                      rules={[
                        {
                          required: true,
                          message: "",
                        },
                      ]}
                    >
                      <TimePicker />
                    </Form.Item>
                    <MinusCircleOutlined onClick={() => remove(name)} />
                  </Space>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Add field
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
          <Map
            defaultState={{ center: [56.322972, 43.997492], zoom: 9 }}
            width={0}
            height={0}
            modules={["geocode", "SuggestView", "suggest"]}
            onLoad={(yMaps) => {
              // eslint-disable-next-line no-new
              new yMaps.SuggestView("location");
              setYMaps(yMaps);
            }}
          />
        </StyledForm>
      </Modal>
    </>
  );
};
export default AddEventWindow;
