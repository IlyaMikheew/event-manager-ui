import { connect } from "react-redux";
import { createEventRequest } from "../actions";
import AddEventWindow from "../components/add-event-window";

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  createEvent: (event) => {
    dispatch(createEventRequest(event));
  },
});

const AddEventWindowContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddEventWindow);

export default AddEventWindowContainer;
