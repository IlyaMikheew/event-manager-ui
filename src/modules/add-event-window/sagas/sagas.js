import { call, put, takeEvery } from "redux-saga/effects";
import EMService from "../../../services";
import { createEventError, createEventSuccess } from "../actions";

const toBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

const getDateTime = (date, time) => {
  date.hours(time.hours());
  date.minutes(time.minutes());
  date.seconds(time.seconds());
  return date;
};

function* createEventWorker({ event }) {
  const resultEvent = {};
  let base64Image;

  if (!event.timelineItems) {
    resultEvent.timelineItems = [];
  } else {
    resultEvent.timelineItems = event.timelineItems.map((e) => ({
      desc: e.desc,
      time: e.time.unix(),
    }));
  }
  resultEvent.name = event.name;
  resultEvent.description = event.description;
  resultEvent.startDate = getDateTime(event.startDate, event.startTime).unix();
  resultEvent.endDate = getDateTime(event.endDate, event.endTime).unix();
  resultEvent.location = event.location;
  if (typeof event.image === "object") {
    base64Image = yield toBase64(event.image.file.originFileObj);
    resultEvent.image = base64Image.substring(base64Image.indexOf(",") + 1);
  } else {
    resultEvent.image = null;
  }
  const service = new EMService();
  try {
    const data = yield call(service.createEvent, resultEvent);
    yield put(createEventSuccess(data.data));
  } catch (e) {
    yield put(createEventError(e));
  }
}

export function* createEventWatcher() {
  yield takeEvery("CREATE_EVENT_REQUEST", createEventWorker);
}
