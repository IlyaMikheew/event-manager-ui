import { call, takeLatest } from "redux-saga/effects";
import { notification } from "antd";
import EMService from "../../../services/EMService";

function* registrationWorker({ values, onSuccess }) {
  const service = new EMService();
  try {
    const data = yield call(service.createUser, values);
    if (data.status !== 200) {
      throw new Error(data.data);
    } else {
      notification.open({
        message: "OK",
        description: "User created successfully",
        duration: 5,
      });
      onSuccess();
    }
  } catch (e) {
    notification.open({
      message: "Error",
      description: "Creation user error",
      duration: 5,
      type: "error",
    });
  }
}

export default function* registrationWatcher() {
  yield takeLatest("REGISTER_REQUEST", registrationWorker);
}
