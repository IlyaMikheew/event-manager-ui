import { connect } from "react-redux";
import { registrationRequested } from "../actions";
import RegistrationPage from "../components/registration-page";

const mapStateToProps = (state) => ({
  error: state.registrationPage.error,
});

const mapDispatchToProps = (dispatch) => ({
  register: (values, onSuccess) => {
    dispatch(registrationRequested(values, onSuccess));
  },
});

const RegistrationPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationPage);

export default RegistrationPageContainer;
