const initialState = {
  error: null,
};

const registrationPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case "REGISTER_REQUEST":
      return state;
    case "REGISTER_SUCCESS":
      return state;
    case "REGISTER_ERROR":
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};

export default registrationPageReducer;
