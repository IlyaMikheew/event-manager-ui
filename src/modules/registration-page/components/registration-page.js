import React from "react";
import { Form, Input, InputNumber, Button, Radio } from "antd";
import "antd/dist/antd.css";
import styled from "styled-components";
import { Link, useHistory } from "react-router-dom";

const validateMessages = {
  // eslint-disable-next-line no-template-curly-in-string
  required: "${label} is required!",
  types: {
    // eslint-disable-next-line no-template-curly-in-string
    email: "${label} is not valid",
    // eslint-disable-next-line no-template-curly-in-string
    number: "${label} is not a valid number!",
  },
  number: {
    // eslint-disable-next-line no-template-curly-in-string
    range: "${label} must be between ${min} and ${max}",
  },
};

const StyledForm = styled(Form)`
  width: 70%;
  height: 500px;
  min-width: 500px;
  margin: auto;
  display: grid;
`;

const StyledEmailInput = styled(Input)`
  width: 100%;
  max-width: 300px;
  margin-left: 29px;
`;

const StyledFNameInput = styled(Input)`
  width: 100%;
  max-width: 300px;
  margin-left: 27px;
`;

const StyledLNameInput = styled(Input)`
  width: 100%;
  max-width: 300px;
  margin-left: 0px;
`;

const StyledPasswordInput = styled(Input.Password)`
  width: 100%;
  max-width: 300px;
  margin-left: 5px;
`;

const StyledPhoneInput = styled(Input)`
  width: 100%;
  max-width: 300px;
  margin-left: 30px;
`;

const StyledAgeInput = styled(InputNumber)`
  margin-left: 41px;
`;

const StyledTextArea = styled(Input.TextArea)`
  width: 100%;
  max-width: 300px;
  margin-left: 8px;
`;

const StyledRadioGroup = styled(Radio.Group)`
  margin-left: 23px;
`;

const StyledRegistrationPage = styled.div`
  width: 30%;
  margin: auto;
  background-color: white;
  border: 1px solid #1890ff;
  border-radius: 10px;
  min-height: 680px;
  min-width: 580px;
`;

const StyledRegistrationText = styled.p`
  font-size: 20px;
  margin-left: 25%;
  padding-top: 20px;
`;

const StyledButton = styled(Button)`
  margin-left: 18%;
  margin-right: 3%;
`;

const RegistrationPage = ({ register }) => {
  const history = useHistory();
  return (
    <StyledRegistrationPage>
      <StyledRegistrationText>
        Registration on Event Manager
      </StyledRegistrationText>
      <StyledForm
        validateMessages={validateMessages}
        onFinish={(values) => {
          register(values, () => history.push("/login"));
        }}
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              type: "email",
            },
          ]}
          label="Email"
        >
          <StyledEmailInput />
        </Form.Item>

        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              pattern: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{8,}$/,
              message:
                "The password must be longer than 8 characters, contain lowercase and uppercase letters and numbers",
            },
          ]}
          label="Password"
          style={{ justifyContent: "space-between" }}
        >
          <StyledPasswordInput />
        </Form.Item>

        <Form.Item
          name="fname"
          rules={[
            {
              required: true,
              type: "string",
              max: 20,
              message: "Name cannot be longer than 20 characters",
            },
          ]}
          label="Name"
        >
          <StyledFNameInput />
        </Form.Item>

        <Form.Item
          name="lname"
          rules={[
            {
              required: true,
              type: "string",
              max: 20,
              message: "Last name cannot be longer than 20 characters",
            },
          ]}
          label="Last Name"
        >
          <StyledLNameInput />
        </Form.Item>
        <Form.Item
          name="age"
          rules={[
            {
              required: true,
              type: "number",
              max: 120,
              min: 0,
            },
          ]}
          label="Age"
        >
          <StyledAgeInput />
        </Form.Item>

        <Form.Item
          name="gender"
          rules={[
            {
              required: true,
              message: "Choose your gender",
            },
          ]}
          label="Gender"
        >
          <StyledRadioGroup>
            <Radio value="m">male</Radio>
            <Radio value="f">female</Radio>
          </StyledRadioGroup>
        </Form.Item>
        <Form.Item
          name="description"
          label="About me"
          rules={[
            {
              required: true,
              type: "string",
              max: 400,
            },
          ]}
        >
          <StyledTextArea rows={5} autoSize={{ minRows: 4, maxRows: 6 }} />
        </Form.Item>
        <Form.Item
          name="phone"
          rules={[
            {
              required: true,
              type: "string",
              len: 10,
              pattern: /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/,
              message: "You must enter a phone number",
            },
          ]}
          label="Phone"
        >
          <StyledPhoneInput addonBefore="+7" />
        </Form.Item>
        <Form.Item>
          <StyledButton type="primary" htmlType="submit">
            Create account
          </StyledButton>
          <Link to={"/login"}>Back to login</Link>
        </Form.Item>
      </StyledForm>
    </StyledRegistrationPage>
  );
};

export default RegistrationPage;
