export const registrationRequested = (values, onSuccess) => ({
  type: "REGISTER_REQUEST",
  values,
  onSuccess,
});

export const registrationSuccess = () => ({
  type: "REGISTER_SUCCESS",
});

export const registrationError = (error) => ({
  type: "REGISTER_ERROR",
  payload: error,
});
