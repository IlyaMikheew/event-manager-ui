import { put, call, takeEvery } from "redux-saga/effects";
import EMService from "../../../services/EMService";
import { eventsLoaded, eventsError } from "../actions";

function* fetchEvents({ filter, pagination, sort }) {
  const service = new EMService();
  try {
    const values = {};
    values.filter = filter;
    values.pagination = pagination;
    values.sort = sort;
    const data = yield call(service.getEvents, values);
    yield put(eventsLoaded(data.data));
  } catch (e) {
    yield put(eventsError(e));
  }
}

export function* fetchEventsWatcher() {
  yield takeEvery("FETCH_EVENTS_REQUEST", fetchEvents);
}
