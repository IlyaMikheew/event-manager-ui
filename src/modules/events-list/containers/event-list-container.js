import { connect } from "react-redux";
import { eventsRequested } from "../actions";
import EventList from "../components/event-list";
import { subscribeRequest, unsubscribeRequest } from "../../event-page/actions";

const mapStateToProps = (state) => ({
  events: state.eventList.events,
  filter: state.eventList.filter,
  pagination: state.eventList.pagination,
  sort: state.eventList.sort,
  total: state.eventList.total,
  error: state.eventList.error,
  loading: state.eventList.loading,
  subscribedEvents: state.eventList.subscribedEvents,
  subscriptionCount: state.eventList.subscriptionCount,
});

const mapDispatchToProps = (dispatch) => ({
  getEvents: (filter, pagination, sort) => {
    dispatch(eventsRequested(filter, pagination, sort));
  },
  subscribe: (eventId) => {
    dispatch(subscribeRequest(eventId));
  },
  unsubscribe: (eventId) => {
    dispatch(unsubscribeRequest(eventId));
  },
});

const EventListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EventList);

export default EventListContainer;
