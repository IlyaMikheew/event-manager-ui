import React, { useCallback, useEffect, useState } from "react";
import { Table, Radio, Spin } from "antd";
import {
  HeartOutlined,
  CompassOutlined,
  EditOutlined,
  HeartFilled,
} from "@ant-design/icons";
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import { Link, useHistory } from "react-router-dom";
import Cookies from "universal-cookie";
import styled from "styled-components";
import EditEventWindowContainer from "../../edit-event-window/containers/edit-event-window-container";

const cookies = new Cookies();

const StyledHeartOutlined = styled(HeartOutlined)`
  margin: 0 5px;
  font-size: 22px;
  cursor: pointer;
`;

const StyledCompassOutlined = styled(CompassOutlined)`
  margin: 0 5px;
  font-size: 22px;
`;

const StyledEditOutlined = styled(EditOutlined)`
  margin: 0 5px;
  font-size: 22px;
`;

const StyledHeartFilled = styled(HeartFilled)`
  margin: 0 5px;
  font-size: 22px;
  color: red;
  cursor: pointer;
`;

const EventList = ({
  events,
  filter,
  pagination,
  sort,
  getEvents,
  total,
  loading,
  error,
  subscribe,
  unsubscribe,
  subscribedEvents,
  // eslint-disable-next-line no-unused-vars
  subscriptionCount,
}) => {
  // eslint-disable-next-line no-unused-vars
  const userId = Number(cookies.get("userId"));
  const history = useHistory();

  const [isEditEventVisible, setIsEditEventVisible] = useState(false);
  const [editedEventId, setEditedEventId] = useState(-1);

  useEffect(() => {
    if (!cookies.get("userId")) {
      history.push("/login");
      return;
    }
    getEvents(filter, pagination, sort);
    // eslint-disable-next-line
  }, [subscriptionCount]);

  useEffect(() => {
    if (!cookies.get("userId")) {
      history.push("/login");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (error !== null) {
      throw error;
    }
  }, [error]);

  const onChange = useCallback(
    (page, f, sorter) => {
      let newPagination = {
        ...pagination,
        start: (page.current - 1) * page.pageSize,
        page: page.current,
      };
      let newSort;
      let direction = "ascend";
      let property = "name";
      if (sorter.order !== undefined) {
        if (sorter.order !== sort.direction) {
          direction = sorter.order;
          property = sorter.field;
          newSort = { property, direction };
          newPagination = { start: 0, limit: 13, page: 1 };
          getEvents(
            filter,
            { start: 0, limit: 13, page: 1 },
            { property, direction }
          );
          return;
        }
        direction = sorter.order;
        property = sorter.field;
        newSort = { property, direction };
      }
      newSort = newSort || sort;
      getEvents(
        filter,
        {
          ...newPagination,
          start: (page.current - 1) * page.pageSize,
          page: page.current,
        },
        newSort
      );
    },
    // eslint-disable-next-line
    [filter, pagination, sort]
  );

  const columns = [
    {
      title: () => "Name",
      dataIndex: "name",
      // eslint-disable-next-line react/display-name
      render: (text, record) => <Link to={`/event/${record.id}`}>{text}</Link>,
      sorter: true,
    },
    {
      title: "Date",
      dataIndex: "startDate",
      sorter: true,
      render: (text) => text.replace("T", " "),
    },
    {
      title: "Address",
      dataIndex: "address",
    },
    {
      title: "Actions",
      dataIndex: "actions",
      // eslint-disable-next-line react/display-name,no-unused-vars
      render: (text, record) => (
        <>
          <StyledCompassOutlined
            onClick={() => {
              window.location.href = `/home?eventId=${record.id}`;
            }}
          />
          {subscribedEvents.find((e) => e.id === record.id) ? (
            <StyledHeartFilled onClick={() => unsubscribe(record.id)} />
          ) : (
            <StyledHeartOutlined onClick={() => subscribe(record.id)} />
          )}
          {userId === record.author ? (
            <StyledEditOutlined
              onClick={() => {
                setEditedEventId(record.id);
                setIsEditEventVisible(true);
              }}
            />
          ) : (
            <></>
          )}
        </>
      ),
    },
  ];

  return (
    <Spin spinning={loading}>
      <div>
        <Radio.Group
          value={filter.value}
          onChange={(e) => {
            console.log(e);
          }}
        >
          <Radio
            value="all"
            onClick={() => {
              const newFilter = {
                ...filter,
                value: "all",
              };
              getEvents(newFilter, { ...pagination, page: 1 }, sort);
            }}
          >
            All
          </Radio>
          <Radio
            value="favorites"
            onClick={() => {
              const newFilter = {
                ...filter,
                value: "favorites",
              };
              getEvents(newFilter, { ...pagination, page: 1 }, sort);
            }}
          >
            Favorites
          </Radio>
          <Radio
            value="my"
            onClick={() => {
              const newFilter = {
                ...filter,
                value: "my",
              };
              getEvents(newFilter, { ...pagination, page: 1 }, sort);
            }}
          >
            My Events
          </Radio>
          <Radio
            value="finished"
            onClick={() => {
              const newFilter = {
                ...filter,
                value: "finished",
              };
              getEvents(newFilter, { ...pagination, page: 1 }, sort);
            }}
          >
            Finished
          </Radio>
        </Radio.Group>
        <br />
        <br />
        <Table
          columns={columns}
          onChange={onChange}
          dataSource={events}
          rowKey="id"
          size="middle"
          pagination={{
            current: pagination.page,
            pageSize: pagination.limit,
            total,
            showSizeChanger: false,
          }}
        />
        {isEditEventVisible ? (
          <EditEventWindowContainer
            isVisible={isEditEventVisible}
            setVisible={setIsEditEventVisible}
            eventId={editedEventId}
          />
        ) : (
          <></>
        )}
      </div>
    </Spin>
  );
};

EventList.propTypes = {
  events: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.number,
      address: PropTypes.string,
      date: PropTypes.string,
      name: PropTypes.string,
    })
  ),
};

export default EventList;
