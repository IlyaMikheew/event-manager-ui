const eventsRequested = (filter, pagination, sort) => ({
  type: "FETCH_EVENTS_REQUEST",
  filter,
  pagination,
  sort,
});

const eventsLoaded = (events) => ({
  type: "FETCH_EVENTS_SUCCESS",
  payload: events,
});

const eventsError = (error) => ({
  type: "FETCH_EVENTS_FAILURE",
  payload: error,
});

export { eventsRequested, eventsLoaded, eventsError };
