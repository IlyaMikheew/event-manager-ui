const initialState = {
  events: [],
  loading: false,
  error: null,
  filter: {
    operator: "eq",
    value: "all",
    property: "type",
  },
  pagination: {
    start: 0,
    limit: 13,
    page: 1,
  },
  sort: {
    property: "name",
    direction: "ascend",
  },
  total: 0,
  subscribedEvents: [],
  subscriptionCount: 0,
};

const eventListReducer = (state = initialState, action) => {
  const newCount = state.subscriptionCount;
  switch (action.type) {
    case "FETCH_EVENTS_REQUEST":
      return {
        ...state,
        loading: true,
        pagination: action.pagination,
        sort: action.sort,
        filter: action.filter,
      };
    case "FETCH_EVENTS_SUCCESS":
      return {
        ...state,
        events: action.payload.page.content,
        loading: false,
        total: action.payload.page.totalElements,
        subscribedEvents: action.payload.subscribedEvents,
        subscriptionCount: action.payload.subscribedEvents.length,
      };
    case "SUBSCRIBE_EVENT_SUCCESS":
      return {
        ...state,
        subscriptionCount: newCount + 1,
      };
    case "UNSUBSCRIBE_EVENT_SUCCESS":
      return {
        ...state,
        subscriptionCount: newCount - 1,
      };
    case "FETCH_EVENTS_FAILURE":
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default eventListReducer;
