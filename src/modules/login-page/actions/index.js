export const loginRequested = (values, onSuccess) => ({
  type: "LOGIN_REQUEST",
  values,
  onSuccess,
});

export const loginSuccess = (token, email) => ({
  type: "LOGIN_SUCCESS",
  email,
  token,
});

export const loginError = (error) => ({
  type: "LOGIN_ERROR",
  payload: error,
});
