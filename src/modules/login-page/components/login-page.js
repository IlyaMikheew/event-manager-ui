import React from "react";
import { Link, useHistory } from "react-router-dom";
import { Form, Input, Button } from "antd";
import "antd/dist/antd.css";
import styled from "styled-components";
import PropTypes from "prop-types";

const StyledButton = styled(Button)`
  margin-right: 0px;
  width: 120px;
`;

const StyledSubmitButton = styled(Button)`
  margin-right: 33px;
  min-width: 30px;
  width: 100px;
`;

const StyledForm = styled(Form)`
  margin-left: 5%;
  min-width: 350px;
  max-height: 150px;
`;

const StyledInput = styled(Input)`
  width: 79%;
  max-width: 260px;
  height: 30px;
  margin-left: 25px;
`;

const StyledInputPassword = styled(Input.Password)`
  width: 86%;
  max-width: 260px;
  height: 30px;
`;

const StyledButtonsItem = styled(Form.Item)`
  width: 80%;
  margin-left: 70px;
`;

const StyledLoginPage = styled.div`
  min-width: 350px;
  width: 20%;
  height: 29%;
  margin: auto;
  background-color: white;
  border: 1px solid #1890ff;
  border-radius: 10px;
  border-radius: 10px;
`;

const StyledLoginText = styled.p`
  font-size: 20px;
  margin-left: 23%;
  padding-top: 20px;
`;

const StyledLoginError = styled.p`
  color: red;
  margin-left: 70px;
`;

const LoginPage = ({ login, error }) => {
  const history = useHistory();
  return (
    <StyledLoginPage>
      <StyledLoginText>
        Login to <span color={"blue"}>E</span>vent manager
      </StyledLoginText>
      <StyledForm
        onFinish={(values) => {
          login(values, () => history.push("/home"));
        }}
      >
        <Form.Item name="email" label="Email">
          <StyledInput />
        </Form.Item>
        <Form.Item name="password" label="Password">
          <StyledInputPassword />
        </Form.Item>
        {error && (
          <StyledLoginError>Incorrect login or password</StyledLoginError>
        )}
        <StyledButtonsItem>
          <StyledSubmitButton htmlType="submit" type="primary">
            Login
          </StyledSubmitButton>
          <Link to="/registration">
            <StyledButton htmlType="button">Create account</StyledButton>
          </Link>
        </StyledButtonsItem>
      </StyledForm>
    </StyledLoginPage>
  );
};

LoginPage.propTypes = {
  isLoggedIn: PropTypes.bool,
  onLogin: PropTypes.func,
};

export default LoginPage;
