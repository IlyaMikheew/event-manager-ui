import { put, call, takeLatest } from "redux-saga/effects";
import Cookies from "universal-cookie";
import EMService from "../../../services/EMService";
import { loginSuccess, loginError } from "../actions";

function* loginWorker({ values, onSuccess }) {
  const service = new EMService();
  const cookies = new Cookies();
  try {
    const data = yield call(service.login, values);
    yield put(loginSuccess(data.data));
    cookies.set("token", data.data.token, { maxAge: 3600 });
    cookies.set("email", data.data.username, { maxAge: 3600 });
    cookies.set("userId", data.data.userId, { maxAge: 3600 });
    onSuccess();
  } catch (e) {
    yield put(loginError(e));
  }
}

export function* loginWatcher() {
  yield takeLatest("LOGIN_REQUEST", loginWorker);
}
