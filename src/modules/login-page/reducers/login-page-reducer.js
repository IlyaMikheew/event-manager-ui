const initialState = {
  token: null,
  email: null,
  error: false,
};

const loginPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOGIN_REQUEST":
      return state;
    case "LOGIN_SUCCESS":
      return {
        ...state,
        token: action.token,
        email: action.email,
      };
    case "LOGIN_ERROR":
      return {
        ...state,
        error: true,
      };
    default:
      return state;
  }
};

export default loginPageReducer;
