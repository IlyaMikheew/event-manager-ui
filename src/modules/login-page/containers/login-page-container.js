import { connect } from "react-redux";
import { loginRequested } from "../actions";
import LoginPage from "../components/login-page";

const mapStateToProps = (state) => ({
  token: state.loginPage.token,
  email: state.loginPage.email,
  error: state.loginPage.error,
});

const mapDispatchToProps = (dispatch) => ({
  login: (values, onSuccess) => {
    dispatch(loginRequested(values, onSuccess));
  },
});

const LoginPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);

export default LoginPageContainer;
