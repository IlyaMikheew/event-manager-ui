import React, { useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { Row, Timeline, Image, Col, Avatar, Tooltip, Spin } from "antd";
import { HeartOutlined, CompassOutlined, HeartFilled } from "@ant-design/icons";
import Cookies from "universal-cookie";
import "antd/dist/antd.css";
import styled from "styled-components";
import StyledPage from "../../../components/styled-components/StyledPage";
import CommentsBlockContainer from "../../comments-block/containers/comments-block-container";
import HeaderContainer from "../../header/containers/header-container";

const cookies = new Cookies();

const Left = styled.div`
  width: 35%;
  min-height: 870px;
  border-right: 1px solid #d9d9d9;
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
`;

const Right = styled.div`
  width: 60%;
  height: 98%;
  display: flex;
  flex-direction: column;
  margin-left: 40px;
  margin-top: 20px;
`;

const EventTitle = styled.div`
  font-size: 22px;
  font-weight: 500;
  margin: 15px auto 0 20px;
`;

const TimelineWrapper = styled.div`
  width: 300px;
  margin: 10px auto;
`;

const StyleDateTime = styled.div`
  max-width: 350px;
  min-height: 40px;
  font-size: 18px;
  letter-spacing: 0.03em;
  font-weight: 500;
`;

const StyledAddress = styled.div`
  min-width: 300px;
  min-height: 40px;
  font-size: 19px;
  letter-spacing: 0.03em;
  font-weight: 500;
  margin-right: 10px;
`;

const StyledAvatarGroup = styled(Avatar.Group)`
  margin-bottom: 20px;
`;

const StyledDescription = styled.div`
  min-width: 300px;
  min-height: 100px;
  max-height: 180px;
  font-size: 14px;
  overflow: auto;
  margin-right: 10px;
`;

const StyledHeartOutlined = styled(HeartOutlined)`
  padding: 10px 7px;
  font-size: 18px;
  cursor: pointer;
`;

const StyledCompassOutlined = styled(CompassOutlined)`
  padding-top: 10px;
  font-size: 18px;
`;

const StyledHeartFilled = styled(HeartFilled)`
  padding: 10px 7px;
  font-size: 18px;
  color: red;
  cursor: pointer;
`;

const StyledUsersTitle = styled.p`
  font-size: 15px;
  padding-top: 5px;
  padding-right: 5px;
  font-weight: 500;
`;

const StyledImageWrapper = styled.div`
  width: 95%;
  height: 350px;
  max-width: 100%;
`;

const EventPage = ({
  event,
  timelineItems,
  users,
  getEvent,
  match,
  subscribe,
  unsubscribe,
  error,
  loading,
}) => {
  const history = useHistory();
  useEffect(() => {
    if (!cookies.get("userId")) {
      history.push("/login");
      return;
    }
    getEvent(match.params.eventId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getEvent, match.params.eventId]);

  useEffect(() => {
    if (error !== null) {
      throw error;
    }
  }, [error]);

  const timeline = timelineItems.map((i) => (
    <Timeline.Item label={i.time.split("T")[1]} key={i.id}>
      <p>{i.description}</p>
    </Timeline.Item>
  ));

  const usersEmails = users.map((i) => i.email);

  const usersInfo = users.map((i) => (
    // eslint-disable-next-line react/jsx-key
    <Link to={`/user/${i.id}`} key={i.id}>
      <Avatar
        src={
          i.avatar
            ? `data:image/png;base64,${i.avatar}`
            : "https://windows10free.ru/uploads/posts/2017-04/1493287748_1487679899_icon-user-640x640.png"
        }
      />
    </Link>
  ));

  return (
    <>
      <HeaderContainer />
      <StyledPage>
        <Spin spinning={loading}>
          <Row>
            <Left>
              <EventTitle>
                <Row>
                  <p>{event.name}</p>
                  <Tooltip
                    key="event-subscribe"
                    title={
                      usersEmails.includes(cookies.get("email"))
                        ? "Unsubscribe"
                        : "Subscribe"
                    }
                  >
                    <span
                      onClick={() =>
                        usersEmails.includes(cookies.get("email"))
                          ? unsubscribe(match.params.eventId)
                          : subscribe(match.params.eventId)
                      }
                    >
                      {usersEmails.includes(cookies.get("email")) ? (
                        <StyledHeartFilled />
                      ) : (
                        <StyledHeartOutlined />
                      )}
                    </span>
                  </Tooltip>
                  <Link to={`/home?eventId=${event.id}`}>
                    <StyledCompassOutlined />
                  </Link>
                </Row>
              </EventTitle>
              <TimelineWrapper>
                <Timeline mode="right">{timeline}</Timeline>
              </TimelineWrapper>
            </Left>
            <Right>
              <Row>
                <Col span={12}>
                  <StyledAddress>
                    {event.location ? event.location.address : " "}
                  </StyledAddress>
                  <StyleDateTime>
                    Start date/time:{" "}
                    {event.startDate ? event.startDate.replace("T", " ") : " "}
                  </StyleDateTime>
                  <StyledDescription>{event.description}</StyledDescription>
                  <StyledUsersTitle>Subscribed users:</StyledUsersTitle>
                  <StyledAvatarGroup maxCount={10}>
                    {usersInfo}
                  </StyledAvatarGroup>
                </Col>
                <Col span={12}>
                  <StyledImageWrapper>
                    {event.image ? (
                      <Image
                        src={`data:image/png;base64,${event.image}`}
                        width={"100%"}
                        height={"100%"}
                      />
                    ) : (
                      <></>
                    )}
                  </StyledImageWrapper>
                </Col>
              </Row>
              <CommentsBlockContainer eventId={match.params.eventId} />
            </Right>
          </Row>
        </Spin>
      </StyledPage>
    </>
  );
};

export default EventPage;
