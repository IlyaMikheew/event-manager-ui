export const eventRequested = (eventId) => ({
  type: "FETCH_EVENT_REQUEST",
  eventId,
});

export const eventLoaded = (event, timelineItems, users) => ({
  type: "FETCH_EVENT_SUCCESS",
  event,
  timelineItems,
  users,
});

export const eventError = (error) => ({
  type: "FETCH_EVENT_ERROR",
  error,
});

export const subscribeRequest = (eventId) => ({
  type: "SUBSCRIBE_EVENT_REQUEST",
  eventId,
});

export const subscribeSuccess = (users) => ({
  type: "SUBSCRIBE_EVENT_SUCCESS",
  users,
});
export const unsubscribeRequest = (eventId) => ({
  type: "UNSUBSCRIBE_EVENT_REQUEST",
  eventId,
});

export const unsubscribeSuccess = (users) => ({
  type: "UNSUBSCRIBE_EVENT_SUCCESS",
  users,
});
