const initialState = {
  event: {},
  comments: [],
  timelineItems: [],
  error: null,
  users: [],
  loading: false,
};

const eventPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_EVENT_REQUEST":
      return {
        ...state,
        loading: true,
      };
    case "FETCH_EVENT_SUCCESS":
      return {
        ...state,
        event: action.event.event,
        timelineItems: action.event.timelineItems,
        users: action.event.users,
        loading: false,
      };
    case "FETCH_EVENT_ERROR":
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case "SUBSCRIBE_EVENT_REQUEST":
      return {
        ...state,
      };
    case "SUBSCRIBE_EVENT_SUCCESS":
      return {
        ...state,
        users: action.users,
      };
    case "UNSUBSCRIBE_EVENT_REQUEST":
      return {
        ...state,
      };
    case "UNSUBSCRIBE_EVENT_SUCCESS":
      return {
        ...state,
        users: action.users,
      };
    default:
      return state;
  }
};

export default eventPageReducer;
