import { connect } from "react-redux";
import {
  eventRequested,
  subscribeRequest,
  unsubscribeRequest,
} from "../actions";
import EventPage from "../components/event-page";

const mapStateToProps = (state) => ({
  event: state.eventPage.event,
  comments: state.eventPage.comments,
  timelineItems: state.eventPage.timelineItems,
  users: state.eventPage.users,
  error: state.eventPage.error,
  loading: state.eventPage.loading,
});

const mapDispatchToProps = (dispatch) => ({
  getEvent: (id) => {
    dispatch(eventRequested(id));
  },
  subscribe: (eventId) => {
    dispatch(subscribeRequest(eventId));
  },
  unsubscribe: (eventId) => {
    dispatch(unsubscribeRequest(eventId));
  },
});

const EventPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EventPage);

export default EventPageContainer;
