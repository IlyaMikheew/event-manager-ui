import { put, call, takeEvery } from "redux-saga/effects";
import { notification } from "antd";
import EMService from "../../../services/EMService";
import {
  eventLoaded,
  eventError,
  subscribeSuccess,
  unsubscribeSuccess,
} from "../actions";

function* fetchEvent({ eventId }) {
  const service = new EMService();
  try {
    const data = yield call(service.getEvent, eventId);
    yield put(eventLoaded(data.data));
  } catch (e) {
    yield put(eventError(e));
  }
}

function* subscribeEvent({ eventId }) {
  const service = new EMService();
  try {
    const data = yield call(service.subscribe, eventId);
    yield put(subscribeSuccess(data.data.users));
  } catch (e) {
    notification.open({
      message: "Error",
      description: "Unable to subscribe",
      duration: 5,
      type: "error",
    });
  }
}

function* unsubscribeEvent({ eventId }) {
  const service = new EMService();
  try {
    const data = yield call(service.unsubscribe, eventId);
    yield put(unsubscribeSuccess(data.data.users));
  } catch (e) {
    notification.open({
      message: "Error",
      description: "Unable to unsubscribe",
      duration: 5,
      type: "error",
    });
  }
}

export function* fetchEventWatcher() {
  yield takeEvery("FETCH_EVENT_REQUEST", fetchEvent);
}

export function* subscribeEventWatcher() {
  yield takeEvery("SUBSCRIBE_EVENT_REQUEST", subscribeEvent);
}

export function* unsubscribeEventWatcher() {
  yield takeEvery("UNSUBSCRIBE_EVENT_REQUEST", unsubscribeEvent);
}
