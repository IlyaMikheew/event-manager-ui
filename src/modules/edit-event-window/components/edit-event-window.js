import React, { useEffect, useState } from "react";
import {
  Button,
  DatePicker,
  Form,
  Input,
  Modal,
  Row,
  Spin,
  TimePicker,
  Upload,
} from "antd";
import styled from "styled-components";
import { UploadOutlined } from "@ant-design/icons";
import moment from "moment";
import { Map } from "react-yandex-maps";

// eslint-disable-next-line no-unused-vars
const StyledForm = styled(Form)`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding-right: 30px;
  min-width: 500px;
`;

const StyledTextArea = styled(Input.TextArea)`
  width: 400px;
`;

const StyledDatePicker = styled(DatePicker)`
  margin-left: 20px;
`;

const StyledEndDatePicker = styled(DatePicker)`
  margin-left: 25px;
`;

const StyledTimePicker = styled(TimePicker)`
  margin-left: 6px;
`;

const DateTimeWrapper = styled(Form.Item)`
  display: inline-block;
  margin-bottom: 0px;
`;

const StyledNameInput = styled(Input)`
  margin-left: 32px;
  width: 300px;
`;

const StyledLocationInput = styled.input`
  margin-left: 32px;
  width: 400px;
  height: 32px;
  margin-bottom: 30px;
`;

const StyledLocationWrapper = styled.div`
  display: flex;
`;

const StyledUpload = styled(Upload)`
  margin-left: 4px;
`;

const geocode = async (ymaps, address) => {
  const response = await ymaps.geocode(address);
  const coordinates = await response.geoObjects
    .get(0)
    .geometry.getCoordinates();
  return coordinates;
};

const EditEventWindow = ({
  event,
  // eslint-disable-next-line no-unused-vars
  timelineItems,
  eventId,
  getEvent,
  isVisible,
  setVisible,
  editEvent,
  loading,
}) => {
  const [form] = Form.useForm();
  // eslint-disable-next-line no-unused-vars
  const [editedItems, setEditedItems] = useState();
  const [ymaps, setYMaps] = useState(null);

  useEffect(() => {
    getEvent(eventId);
  }, [eventId, getEvent]);

  useEffect(() => {
    setEditedItems(timelineItems);
  }, [timelineItems]);

  const onFinish = async () => {
    const editedEvent = form.getFieldsValue();
    const location = {};
    if (!editedEvent.image) {
      editedEvent.image = event.image;
    }
    if (!document.getElementById("location").value) {
      editedEvent.location = location;
      editedEvent.location.address = event.location.address;
      editedEvent.location.coordinates = [
        event.location.xcords,
        event.location.ycords,
      ];
    } else {
      location.address = document.getElementById("location").value;
      location.coordinates = await geocode(ymaps, location.address);
      editedEvent.location = location;
    }
    editedEvent.id = event.id;
    editedEvent.author = event.author;
    editEvent(editedEvent);
    setVisible(false);
    form.resetFields();
  };

  return (
    <>
      <Modal
        visible={isVisible}
        width={700}
        title="Edit Event"
        onOk={() => {
          form.submit();
        }}
        onCancel={() => {
          setVisible(false);
          form.resetFields();
        }}
        confirmLoading={loading}
      >
        {event === null ? (
          <></>
        ) : (
          <Spin spinning={loading}>
            <Form
              form={form}
              initialValues={{
                name: event.name,
                description: event.description,
                startDate: moment(event.startDate),
                startTime: moment(event.startDate),
                endDate: moment(event.endDate),
                endTime: moment(event.endDate),
              }}
              onFinish={onFinish}
            >
              <Form.Item name="image" label="Event image">
                <StyledUpload
                  method="GET"
                  listType="picture"
                  maxCount={1}
                  accept={"image/*"}
                >
                  <Button icon={<UploadOutlined />}>Upload image</Button>
                </StyledUpload>
              </Form.Item>
              <Form.Item
                name="name"
                label="Name"
                rules={[
                  {
                    required: true,
                    type: "string",
                    max: 60,
                    message: "",
                  },
                ]}
              >
                <StyledNameInput />
              </Form.Item>
              <Form.Item
                name="description"
                label="Description"
                rules={[
                  {
                    required: true,
                    type: "string",
                    message: "",
                  },
                ]}
              >
                <StyledTextArea rows={5} />
              </Form.Item>
              <Form.Item label="Start date">
                <Row>
                  <DateTimeWrapper
                    name="startDate"
                    rules={[
                      {
                        required: true,
                        message: "input date",
                      },
                    ]}
                  >
                    <StyledDatePicker />
                  </DateTimeWrapper>
                  <DateTimeWrapper
                    name="startTime"
                    rules={[
                      {
                        required: true,
                        message: "input time",
                      },
                    ]}
                  >
                    <StyledTimePicker />
                  </DateTimeWrapper>
                </Row>
              </Form.Item>
              <Form.Item label="End date">
                <Row>
                  <DateTimeWrapper
                    name="endDate"
                    rules={[
                      {
                        required: true,
                        message: "input date",
                      },
                    ]}
                  >
                    <StyledEndDatePicker />
                  </DateTimeWrapper>
                  <DateTimeWrapper
                    name="endTime"
                    rules={[
                      {
                        required: true,
                        message: "input time",
                      },
                    ]}
                  >
                    <StyledTimePicker />
                  </DateTimeWrapper>
                </Row>
              </Form.Item>
              <StyledLocationWrapper>
                <p>Location: </p>
                <StyledLocationInput
                  className="ant-input"
                  type="text"
                  id="location"
                  placeholder={event.location.address}
                />
              </StyledLocationWrapper>
              <Map
                defaultState={{ center: [56.322972, 43.997492], zoom: 9 }}
                width={0}
                height={0}
                modules={["geocode", "SuggestView", "suggest"]}
                onLoad={(yMaps) => {
                  // eslint-disable-next-line no-new
                  new yMaps.SuggestView("location");
                  setYMaps(yMaps);
                }}
              />
            </Form>
          </Spin>
        )}
      </Modal>
    </>
  );
};

export default EditEventWindow;
