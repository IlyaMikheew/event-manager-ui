import { connect } from "react-redux";
import { editEventRequest, fetchEventRequest } from "../actions";
import EditEventWindow from "../components/edit-event-window";

const mapStateToProps = (state) => ({
  event: state.editEventWindow.event,
  timelineItems: state.editEventWindow.timelineItems,
  loading: state.editEventWindow.loading,
});

const mapDispatchToProps = (dispatch) => ({
  editEvent: (event) => {
    dispatch(editEventRequest(event));
  },
  getEvent: (eventId) => {
    dispatch(fetchEventRequest(eventId));
  },
});

const EditEventWindowContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditEventWindow);

export default EditEventWindowContainer;
