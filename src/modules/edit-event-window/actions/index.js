const editEventRequest = (editedEvent) => ({
  type: "EDIT_EVENT_REQUEST",
  editedEvent,
});

const editEventSuccess = () => ({
  type: "EDIT_EVENT_SUCCESS",
});

const editEventError = (error) => ({
  type: "EDIT_EVENT_ERROR",
  error,
});

const fetchEventRequest = (eventId) => ({
  type: "FETCH_EDIT_EVENT_REQUEST",
  eventId,
});

const fetchEventSuccess = (event) => ({
  type: "FETCH_EDIT_EVENT_SUCCESS",
  event,
});

const fetchEventError = (error) => ({
  type: "FETCH_EDIT_EVENT_ERROR",
  error,
});

export {
  editEventError,
  editEventRequest,
  editEventSuccess,
  fetchEventSuccess,
  fetchEventRequest,
  fetchEventError,
};
