const initialState = {
  editedEvent: null,
  event: null,
  error: null,
  timelineItems: [],
  loading: false,
};

const editEventReducer = (state = initialState, action) => {
  switch (action.type) {
    case "EDIT_EVENT_REQUEST":
      return {
        ...state,
        editedEvent: action.editEvent,
        loading: true,
      };
    case "EDIT_EVENT_SUCCESS":
      return {
        ...state,
        loading: false,
      };
    case "EDIT_EVENT_ERROR":
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case "FETCH_EDIT_EVENT_REQUEST":
      return {
        ...state,
        loading: true,
        eventId: action.eventId,
      };
    case "FETCH_EDIT_EVENT_SUCCESS":
      return {
        ...state,
        event: action.event.event,
        loading: false,
        timelineItems: action.event.timelineItems,
      };
    case "FETCH_EDIT_EVENT_ERROR":
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
export default editEventReducer;
