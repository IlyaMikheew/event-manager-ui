import { call, put, takeEvery } from "redux-saga/effects";
import { notification } from "antd";
import EMService from "../../../services";
import { editEventSuccess, fetchEventSuccess } from "../actions";

const toBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

const getDateTime = (date, time) => {
  date.hours(time.hours());
  date.minutes(time.minutes());
  date.seconds(time.seconds());
  return date;
};

function* editEventWorker({ editedEvent }) {
  const resultEvent = {};
  let base64Image;
  if (typeof editedEvent.image === "object") {
    base64Image = yield toBase64(editedEvent.image.file.originFileObj);
    resultEvent.image = base64Image.substring(base64Image.indexOf(",") + 1);
  } else {
    resultEvent.image = editedEvent.image;
  }
  resultEvent.name = editedEvent.name;
  resultEvent.description = editedEvent.description;
  resultEvent.startDate = getDateTime(
    editedEvent.startDate,
    editedEvent.startTime
  ).unix();
  resultEvent.endDate = getDateTime(
    editedEvent.endDate,
    editedEvent.endTime
  ).unix();
  resultEvent.location = editedEvent.location;
  resultEvent.id = editedEvent.id;
  resultEvent.author = editedEvent.author;
  const service = new EMService();
  try {
    const data = yield call(service.editEvent, resultEvent);
    yield put(editEventSuccess(data));
  } catch (e) {
    notification.open({
      message: "Error",
      description: "Unable edit event",
      duration: 5,
      type: "error",
    });
  }
}

function* getEditEventWorker({ eventId }) {
  const service = new EMService();
  try {
    const data = yield call(service.getEvent, eventId);
    yield put(fetchEventSuccess(data.data));
  } catch (e) {
    notification.open({
      message: "Error",
      description: "Unable edit event",
      duration: 5,
      type: "error",
    });
  }
}

export function* editEventWatcher() {
  yield takeEvery("EDIT_EVENT_REQUEST", editEventWorker);
}

export function* fetchEditEventWatcher() {
  yield takeEvery("FETCH_EDIT_EVENT_REQUEST", getEditEventWorker);
}
