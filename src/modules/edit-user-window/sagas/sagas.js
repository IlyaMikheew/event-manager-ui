import { call, put, takeEvery } from "redux-saga/effects";
import { notification } from "antd";
import Cookies from "universal-cookie";
import EMService from "../../../services";
import { userInfoLoaded } from "../actions";
import { disconnect } from "../../../services/web-socket-service";

const cookies = new Cookies();
// eslint-disable-next-line no-unused-vars
const toBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

const logout = () => {
  cookies.remove("token");
  cookies.remove("email");
  cookies.remove("userId");
  window.location.href = `/login`;
  disconnect();
};

function* editUserWorker({ editedUser }) {
  const service = new EMService();
  try {
    // eslint-disable-next-line no-debugger
    debugger;
    if (typeof editedUser.avatar === "object" && editedUser.avatar) {
      const base64Avatar = yield toBase64(editedUser.avatar.file.originFileObj);
      // eslint-disable-next-line
      editedUser.avatar = base64Avatar.substring(base64Avatar.indexOf(",") + 1);
    }
    // eslint-disable-next-line no-unused-vars
    const data = yield call(service.editUser, editedUser);
    logout();
  } catch (e) {
    notification.open({
      message: "Error",
      description: "Unable to edit user",
      duration: 5,
      type: "error",
    });
  }
}

function* fetchUserInfoWorker({ userId }) {
  const service = new EMService();
  try {
    const data = yield call(service.getUser, { userId });
    yield put(userInfoLoaded(data.data));
  } catch (e) {
    notification.open({
      message: "Error",
      description: "Unable to edit user",
      duration: 5,
      type: "error",
    });
  }
}

export function* fetchUserInfoWatcher() {
  yield takeEvery("FETCH_USER_INFO_REQUEST", fetchUserInfoWorker);
}

export function* editUserWatcher() {
  yield takeEvery("EDIT_USER_REQUEST", editUserWorker);
}
