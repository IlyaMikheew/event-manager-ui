const editUserRequest = (editedUser) => ({
  type: "EDIT_USER_REQUEST",
  editedUser,
});

const editUserSuccess = () => ({
  type: "EDIT_USER_SUCCESS",
});

const editUserError = (error) => ({
  type: "EDIT_USER_ERROR",
  error,
});

const userInfoRequested = (userId) => ({
  type: "FETCH_USER_INFO_REQUEST",
  userId,
});

const userInfoLoaded = (user) => ({
  type: "FETCH_USER_INFO_SUCCESS",
  user,
});

export {
  editUserRequest,
  editUserError,
  editUserSuccess,
  userInfoRequested,
  userInfoLoaded,
};
