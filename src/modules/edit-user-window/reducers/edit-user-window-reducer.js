const initialState = {
  user: null,
  editedUser: null,
  loading: false,
  error: null,
};

const editUserReducer = (state = initialState, action) => {
  switch (action.type) {
    case "EDIT_USER_REQUEST":
      return {
        ...state,
        editedUser: action.editedEvent,
        loading: true,
      };
    case "EDIT_USER_SUCCESS":
      return state;
    case "FETCH_USER_INFO_REQUEST":
      return {
        ...state,
        loading: true,
      };
    case "FETCH_USER_INFO_SUCCESS":
      return {
        ...state,
        user: action.user.user,
        loading: false,
      };
    case "EDIT_USER_ERROR":
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};

export default editUserReducer;
