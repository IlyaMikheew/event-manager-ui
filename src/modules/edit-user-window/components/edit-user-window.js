// eslint-disable-next-line no-unused-vars
import React, { useEffect } from "react";
import { Button, Form, Input, Modal, Upload } from "antd";
import Cookies from "universal-cookie";
import styled from "styled-components";
import { UploadOutlined } from "@ant-design/icons";

// eslint-disable-next-line no-unused-vars
const cookies = new Cookies();

const StyledEmailInput = styled(Input)`
  width: 100%;
  max-width: 300px;
  margin-left: 29px;
`;

const StyledFNameInput = styled(Input)`
  width: 100%;
  max-width: 300px;
  margin-left: 27px;
`;

const StyledLNameInput = styled(Input)`
  width: 100%;
  max-width: 300px;
  margin-left: 0px;
`;

const StyledPhoneInput = styled(Input)`
  width: 100%;
  max-width: 300px;
  margin-left: 40px;
`;

const StyledTextArea = styled(Input.TextArea)`
  width: 100%;
  max-width: 300px;
  margin-left: 18px;
`;

const StyledUpload = styled(Upload)`
  margin-left: 34px;
`;

const StyledForm = styled(Form)`
  width: 500px;
`;

const EditUserWindow = ({
  isVisible,
  setVisible,
  user,
  getUser,
  editUser,
  loading,
}) => {
  const [form] = Form.useForm();

  useEffect(() => {
    const userId = cookies.get("userId");
    if (isVisible) {
      getUser(userId);
    }
  }, [getUser, isVisible]);

  const onFinish = () => {
    const editedUser = form.getFieldsValue();
    editedUser.id = user.id;
    if (typeof editedUser.avatar !== "object") {
      editedUser.avatar = user.avatar;
    }
    form.resetFields();
    editUser(editedUser);
    setVisible(false);
  };

  return (
    <Modal
      visible={isVisible}
      width={700}
      title="Edit my profile"
      onOk={() => {
        form.submit();
      }}
      onCancel={() => {
        form.resetFields();
        setVisible(false);
      }}
      confirmLoading={loading}
    >
      {user === null ? (
        <></>
      ) : (
        <StyledForm
          form={form}
          initialValues={{
            email: user.email,
            fname: user.fname,
            lname: user.lname,
            phone: user.phone,
            description: user.description,
          }}
          onFinish={onFinish}
        >
          <Form.Item name="avatar" label="Avatar">
            <StyledUpload
              method="GET"
              listType="picture"
              maxCount={1}
              accept={"image/*"}
            >
              <Button icon={<UploadOutlined />}>Upload image</Button>
            </StyledUpload>
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                type: "email",
              },
            ]}
            label="Email"
          >
            <StyledEmailInput />
          </Form.Item>

          <Form.Item
            name="fname"
            rules={[
              {
                required: true,
                type: "string",
                max: 20,
                message: "Name cannot be longer than 20 characters",
              },
            ]}
            label="Name"
          >
            <StyledFNameInput />
          </Form.Item>

          <Form.Item
            name="lname"
            rules={[
              {
                required: true,
                type: "string",
                max: 20,
                message: "Last name cannot be longer than 20 characters",
              },
            ]}
            label="Last Name"
          >
            <StyledLNameInput />
          </Form.Item>
          <Form.Item name="description" label="About me">
            <StyledTextArea rows={5} />
          </Form.Item>
          <Form.Item
            name="phone"
            rules={[
              {
                type: "string",
                len: 10,
                pattern: /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/,
                message: "You must enter a phone number",
              },
            ]}
            label="Phone"
          >
            <StyledPhoneInput addonBefore="+7" />
          </Form.Item>
        </StyledForm>
      )}
    </Modal>
  );
};

export default EditUserWindow;
