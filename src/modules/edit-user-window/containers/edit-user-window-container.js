import { connect } from "react-redux";
import { editUserRequest, userInfoRequested } from "../actions";
import EditUserWindow from "../components/edit-user-window";

const mapStateToProps = (state) => ({
  user: state.editUserWindow.user,
  loading: state.editUserWindow.loading,
});

const mapDispatchToProps = (dispatch) => ({
  getUser: (userId) => {
    dispatch(userInfoRequested(userId));
  },
  editUser: (user) => {
    dispatch(editUserRequest(user));
  },
});

const EditUserWindowContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditUserWindow);

export default EditUserWindowContainer;
