import { all } from "@redux-saga/core/effects";
import { fetchEventsWatcher } from "./events-list/sagas/sagas";
import { loginWatcher } from "./login-page/sagas/sagas";
import registrationWatcher from "./registration-page/sagas/sagas";
import {
  fetchEventWatcher,
  subscribeEventWatcher,
  unsubscribeEventWatcher,
} from "./event-page/sagas/sagas";
import {
  deleteLikeWatcher,
  fetchCommentsWatcher,
  setCommentWatcher,
  setLikeWatcher,
} from "./comments-block/sagas/sagas";
import { fetchUserWatcher } from "./user-page/sagas/sagas";
import { fetchMapEventsWatcher } from "./main-map/sagas/sagas";
import { searchEventWatcher } from "./header/sagas/sagas";
import {
  editEventWatcher,
  fetchEditEventWatcher,
} from "./edit-event-window/sagas/sagas";
import { createEventWatcher } from "./add-event-window/sagas/sagas";
import {
  editUserWatcher,
  fetchUserInfoWatcher,
} from "./edit-user-window/sagas/sagas";

export function* rootWatcher() {
  yield all([
    fetchEventsWatcher(),
    loginWatcher(),
    registrationWatcher(),
    fetchEventWatcher(),
    fetchCommentsWatcher(),
    setCommentWatcher(),
    fetchUserWatcher(),
    setLikeWatcher(),
    deleteLikeWatcher(),
    subscribeEventWatcher(),
    unsubscribeEventWatcher(),
    fetchMapEventsWatcher(),
    searchEventWatcher(),
    editEventWatcher(),
    fetchEditEventWatcher(),
    createEventWatcher(),
    editUserWatcher(),
    fetchUserInfoWatcher(),
  ]);
}
