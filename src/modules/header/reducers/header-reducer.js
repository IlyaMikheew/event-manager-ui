const initialState = {
  error: null,
  events: [],
};

const headerReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SEARCH_EVENT_REQUEST":
      return state;
    case "SEARCH_EVENT_SUCCESS":
      return {
        ...state,
        events: action.events,
      };
    case "SEARCH_EVENT_ERROR":
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};

export default headerReducer;
