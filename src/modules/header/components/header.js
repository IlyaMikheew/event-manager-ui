import React, { useCallback, useEffect, useState } from "react";
import { Avatar, Input, Button, AutoComplete, Menu, Dropdown } from "antd";
import "antd/dist/antd.css";
import { Link, useHistory } from "react-router-dom";
import styled from "styled-components";
import { UserOutlined, PlusOutlined } from "@ant-design/icons";
import Cookies from "universal-cookie";
import AddEventWindowContainer from "../../add-event-window/containers/add-event-window-container";
// eslint-disable-next-line no-unused-vars
import EditUserWindowContainer from "../../edit-user-window/containers/edit-user-window-container";
import { disconnect } from "../../../services/web-socket-service";

const Title = styled.div`
  font-weight: 500;
  font-size: 28px;
  color: #1890ff;
  letter-spacing: 0.08em;
  padding-left: 20px;
  flex: 2;
`;

const StyledHeaderWrapper = styled.div`
  padding-bottom: 10px;
  width: 80%;
  margin-left: 10%;
  background-color: white;
  min-width: 700px;
  height: 60px;
`;

const StyledHeader = styled.div`
  display: flex;
  flex-direction: row;
`;

const StyledSearchWrapper = styled.div`
  margin-left: 25%;
  margin-top: 8px;
  flex: 5;
`;

const StyledAutoComplete = styled(AutoComplete)`
  width: 100%;
`;

const StyledButton = styled.div`
  margin-top: 8px;
  margin-left: 20%;
  flex: 2;
`;

const StyledAvatar = styled.div`
  padding-top: 5px;
  padding-left: 15px;
  flex: 1;
`;
const cookies = new Cookies();

const Header = ({ events, search }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [isEditUserWindowVisible, setIsEditUserWindowVisible] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const [options, setOptions] = useState([]);
  const history = useHistory();

  const logout = useCallback(() => {
    cookies.remove("token");
    cookies.remove("email");
    cookies.remove("userId");
    history.push("/login");
    disconnect();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (events.length < 5) {
      setOptions(
        events.map((e) => ({
          value: e.name,
          id: e.id,
        }))
      );
    } else {
      setOptions(
        events.slice(0, 5).map((e) => ({
          value: e.name,
          id: e.id,
        }))
      );
    }
  }, [events]);

  const addEvent = () => {
    setIsModalVisible(true);
  };

  const openEditUserWindow = () => {
    setIsEditUserWindowVisible(true);
  };

  const menu = (
    <Menu>
      <Menu.Item key={1}>
        <Link to={`/user/${cookies.get("userId")}`} />
        My profile
      </Menu.Item>
      <Menu.Item onClick={openEditUserWindow} key={2}>
        Edit my profile
      </Menu.Item>
      <Menu.Item danger onClick={logout} key={3}>
        Logout
      </Menu.Item>
    </Menu>
  );

  return (
    <StyledHeaderWrapper>
      <StyledHeader>
        <Link to="/home">
          <Title>EM</Title>
        </Link>
        <StyledSearchWrapper>
          <StyledAutoComplete
            options={options}
            value={searchValue}
            onSearch={(text) => {
              if (text) {
                search(text);
              }
            }}
            onChange={(value) => {
              setSearchValue(value);
            }}
            onSelect={(value, option) => {
              setSearchValue(value);
              history.push("/");
              history.push(`event/${option.id}`);
            }}
          >
            <Input.Search placeholder="Input event name here" />
          </StyledAutoComplete>
        </StyledSearchWrapper>
        <StyledButton icon={<PlusOutlined />} size="middle">
          <Button icon={<PlusOutlined />} size="middle" onClick={addEvent}>
            Add event
          </Button>
        </StyledButton>
        <StyledAvatar>
          <Dropdown overlay={menu}>
            <a className="ant-dropdown-link">
              <Avatar size={35} icon={<UserOutlined />} shape="square" />
            </a>
          </Dropdown>
        </StyledAvatar>
        <AddEventWindowContainer
          isVisible={isModalVisible}
          setVisible={setIsModalVisible}
        />
        <EditUserWindowContainer
          isVisible={isEditUserWindowVisible}
          setVisible={setIsEditUserWindowVisible}
        />
      </StyledHeader>
      <hr color="#D9D9D9" width="98%" size={1} />
    </StyledHeaderWrapper>
  );
};

export default Header;
