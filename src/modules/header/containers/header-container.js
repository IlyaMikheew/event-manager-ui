import { connect } from "react-redux";
import { searchEventRequested } from "../actions";
import Header from "../components/header";

const mapStateToProps = (state) => ({
  events: state.header.events,
  error: state.header.error,
});

const mapDispatchToProps = (dispatch) => ({
  search: (namePart) => {
    dispatch(searchEventRequested(namePart));
  },
});

const HeaderContainer = connect(mapStateToProps, mapDispatchToProps)(Header);

export default HeaderContainer;
