const searchEventRequested = (namePart) => ({
  type: "SEARCH_EVENT_REQUEST",
  namePart,
});

const searchEventSuccess = (events) => ({
  type: "SEARCH_EVENT_SUCCESS",
  events,
});

const searchEvenError = (error) => ({
  type: "SEARCH_EVENT_ERROR",
  error,
});

export { searchEventSuccess, searchEventRequested, searchEvenError };
