import { put, call, takeEvery } from "redux-saga/effects";
import EMService from "../../../services/EMService";
import { searchEvenError, searchEventSuccess } from "../actions";

function* searchEventWorker({ namePart }) {
  const service = new EMService();
  try {
    const data = yield call(service.searchEvent, namePart);
    yield put(searchEventSuccess(data.data));
  } catch (e) {
    yield put(searchEvenError(e));
  }
}

export function* searchEventWatcher() {
  yield takeEvery("SEARCH_EVENT_REQUEST", searchEventWorker);
}
