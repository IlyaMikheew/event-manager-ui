import { call, put, takeEvery } from "redux-saga/effects";
import EMService from "../../../services";
import { userError, userLoaded } from "../actions";

function* fetchUserWorker({ userId, pagination }) {
  const service = new EMService();
  try {
    const data = yield call(service.getUser, { userId, pagination });
    yield put(userLoaded(data.data));
  } catch (e) {
    yield put(userError(e));
  }
}

export function* fetchUserWatcher() {
  yield takeEvery("FETCH_USER_REQUEST", fetchUserWorker);
}
