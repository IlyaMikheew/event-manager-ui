import { connect } from "react-redux";
import { userRequested } from "../actions";
import UserPage from "../components/user-page";

const mapStateToProps = (state) => ({
  user: state.userPage.user,
  error: state.userPage.error,
  loading: state.userPage.loading,
  userEvents: state.userPage.userEvents,
  pagination: state.userPage.pagination,
  total: state.userPage.total,
});

const mapDispatchToProps = (dispatch) => ({
  getUser: (userId, pagination) => {
    dispatch(userRequested(userId, pagination));
  },
});

const UserPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPage);

export default UserPageContainer;
