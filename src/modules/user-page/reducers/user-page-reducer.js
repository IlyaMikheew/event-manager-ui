const initialState = {
  user: {},
  userEvents: [],
  pagination: {
    start: 0,
    limit: 8,
    page: 1,
  },
  total: 0,
  error: null,
  loading: false,
};

const UserPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_USER_REQUEST":
      return {
        ...state,
        loading: true,
        pagination: action.pagination,
      };
    case "FETCH_USER_SUCCESS":
      return {
        ...state,
        user: action.user.user,
        userEvents: action.user.userEvents.content,
        total: action.user.userEvents.totalElements,
        loading: false,
      };
    case "FETCH_USER_ERROR":
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};

export default UserPageReducer;
