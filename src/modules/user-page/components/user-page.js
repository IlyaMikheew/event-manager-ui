import React, { useEffect } from "react";
import { Image, Table, Spin } from "antd";
import styled from "styled-components";
import { Link, useHistory } from "react-router-dom";
import Cookies from "universal-cookie/es6";
import HeaderContainer from "../../header/containers/header-container";

const StyledUserPage = styled.div`
  width: 80%;
  height: 100%;
  margin-left: 10%;
  background-color: white;
  min-width: 700px;
  display: flex;
  flex-direction: column;
`;

const StyledInfoBlock = styled.div`
  display: flex;
  flex: 1;
`;

const StyledTableBlock = styled.div`
  width: 90%;
  margin-left: 5%;
  flex: 2;
`;

const StyledUserInfo = styled.div`
  display: flex;
  padding-left: 20px;
  padding-top: 20px;
  width: 300px;
  height: 300px;
  flex-direction: column;
  flex: 3;
`;

const StyledName = styled.div`
  font-size: 24px;
  flex: 1;
`;

const StyledContactsBlock = styled.div`
  font-size: 16px;
  flex: 4;
`;

const StyledAboutMeBlock = styled.div`
  flex: 7;
  font-size: 15px;
  margin-left: 20px;
  margin-top: 20px;
  margin-right: 5%;
`;

// eslint-disable-next-line no-unused-vars
const StyledImage = styled.div`
  margin-left: 5%;
  flex: 2;
`;

// eslint-disable-next-line no-unused-vars
const cookies = new Cookies();

const UserPage = ({
  user,
  getUser,
  match,
  userEvents,
  pagination,
  total,
  error,
  loading,
}) => {
  const history = useHistory();
  useEffect(() => {
    if (!cookies.get("userId")) {
      history.push("/login");
      return;
    }
    getUser(match.params.userId, pagination);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getUser, match.params.userId, pagination]);

  const columns = [
    {
      title: () => "Name",
      dataIndex: "name",
      // eslint-disable-next-line react/display-name
      render: (text, record) => <Link to={`/event/${record.id}`}>{text}</Link>,
    },
    {
      title: "Date",
      dataIndex: "startDate",
      render: (text) => text.replace("T", " "),
    },
    {
      title: "Address",
      dataIndex: "address",
    },
  ];

  const onChange = (page) =>
    getUser(match.params.userId, {
      ...pagination,
      start: (page.current - 1) * page.pageSize,
      page: page.current,
    });

  useEffect(() => {
    if (error !== null) {
      throw error;
    }
  }, [error]);

  return (
    <>
      <HeaderContainer />
      <StyledUserPage>
        <Spin spinning={loading}>
          <StyledInfoBlock>
            {user.avatar ? (
              <StyledImage>
                <Image
                  src={`data:image/png;base64,${user.avatar}`}
                  width={300}
                  height={300}
                />
              </StyledImage>
            ) : (
              <StyledImage>
                <Image
                  src={
                    "https://windows10free.ru/uploads/posts/2017-04/1493287748_1487679899_icon-user-640x640.png"
                  }
                  width={300}
                  height={300}
                />
              </StyledImage>
            )}
            <StyledUserInfo>
              <StyledName>
                <p>{`${user.fname} ${user.lname}`}</p>
              </StyledName>
              <StyledContactsBlock>
                <p>Contacts: </p>
                <p>Phone: +7 {user.phone}</p>
                <p>Email: {user.email}</p>
              </StyledContactsBlock>
            </StyledUserInfo>
            <StyledAboutMeBlock>
              <StyledName>About me</StyledName>
              <p>{user.description}</p>
            </StyledAboutMeBlock>
          </StyledInfoBlock>
          <StyledTableBlock>
            <StyledName>User events</StyledName>
            <Table
              columns={columns}
              onChange={onChange}
              dataSource={userEvents}
              rowKey="id"
              size="middle"
              pagination={{
                current: pagination.page,
                pageSize: pagination.limit,
                total,
                showSizeChanger: false,
              }}
            />
          </StyledTableBlock>
        </Spin>
      </StyledUserPage>
    </>
  );
};

export default UserPage;
