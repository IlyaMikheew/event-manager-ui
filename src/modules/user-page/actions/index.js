const userRequested = (userId, pagination) => ({
  type: "FETCH_USER_REQUEST",
  userId,
  pagination,
});

const userLoaded = (user) => ({
  type: "FETCH_USER_SUCCESS",
  user,
});

const userError = (error) => ({
  type: "FETCH_USER_ERROR",
  error,
});

export { userRequested, userLoaded, userError };
