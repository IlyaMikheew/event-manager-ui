import { connect } from "react-redux";
import { eventsRequested } from "../actions";
import MainMap from "../components/main-map";
import {
  eventRequested,
  subscribeRequest,
  unsubscribeRequest,
} from "../../event-page/actions";

const mapStateToProps = (state) => ({
  events: state.mainMap.events,
  error: state.mainMap.error,
  subscribedEvents: state.mainMap.subscribedEvents,
  subscriptionCount: state.mainMap.subscriptionCount,
  chosenEvent: state.mainMap.chosenEvent,
});

const mapDispatchToProps = (dispatch) => ({
  getEvents: () => {
    dispatch(eventsRequested());
  },
  subscribe: (eventId) => {
    dispatch(subscribeRequest(eventId));
  },
  unsubscribe: (eventId) => {
    dispatch(unsubscribeRequest(eventId));
  },
  getEvent: (id) => {
    dispatch(eventRequested(id));
  },
});

const MainMapContainer = connect(mapStateToProps, mapDispatchToProps)(MainMap);

export default MainMapContainer;
