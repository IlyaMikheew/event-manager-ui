const eventsRequested = () => ({
  type: "FETCH_MAP_EVENTS_REQUEST",
});

const eventsLoaded = (events) => ({
  type: "FETCH_MAP_EVENTS_SUCCESS",
  events,
});

const eventsError = (error) => ({
  type: "FETCH_MAP_EVENTS_ERROR",
  error,
});

export { eventsRequested, eventsLoaded, eventsError };
