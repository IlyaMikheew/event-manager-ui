import { put, call, takeEvery } from "redux-saga/effects";
import EMService from "../../../services/EMService";
import { eventsError, eventsLoaded } from "../actions";

function* fetchMapEvents() {
  const service = new EMService();
  try {
    const data = yield call(service.getAllEvents);
    yield put(eventsLoaded(data.data));
  } catch (e) {
    yield put(eventsError(e));
  }
}

export function* fetchMapEventsWatcher() {
  yield takeEvery("FETCH_MAP_EVENTS_REQUEST", fetchMapEvents);
}
