const initialState = {
  events: [],
  error: null,
  subscribedEvents: [],
  subscriptionCount: 0,
  chosenEvent: null,
};

const mainMapReducer = (state = initialState, action) => {
  const newCount = state.subscribedCount;
  switch (action.type) {
    case "FETCH_MAP_EVENTS_REQUEST":
      return state;
    case "FETCH_MAP_EVENTS_SUCCESS":
      return {
        ...state,
        events: action.events.events,
        subscribedEvents: action.events.subscribedEvents,
        subscribedCount: action.events.subscribedEvents.length,
        error: null,
      };
    case "FETCH_MAP_EVENTS_ERROR":
      return {
        ...state,
        error: action.error,
      };
    case "SUBSCRIBE_EVENT_SUCCESS":
      return {
        ...state,
        subscriptionCount: newCount + 1,
      };
    case "UNSUBSCRIBE_EVENT_SUCCESS":
      return {
        ...state,
        subscriptionCount: newCount - 1,
      };
    default:
      return state;
    case "FETCH_EVENT_REQUEST":
      return {
        ...state,
      };
    case "FETCH_EVENT_SUCCESS":
      return {
        ...state,
        chosenEvent: action.event.event,
      };
  }
};

export default mainMapReducer;
