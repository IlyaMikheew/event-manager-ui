import React, { useEffect, useState } from "react";
import { Map, Placemark } from "react-yandex-maps";
import EventPanel from "../../../components/event-panel";

const MainMap = ({
  events,
  getEvents,
  chosenEvent,
  getEvent,
  subscribe,
  unsubscribe,
  selectedEventId,
  subscribedEvents,
  subscriptionCount,
}) => {
  const [eventPanelVisible, setEventPanelVisible] = useState(false);
  const [visibleEventId, setVisibleEventId] = useState(-1);
  const [mapState, setMapState] = useState(null);

  useEffect(() => {
    getEvents();
  }, [getEvents, subscriptionCount]);

  useEffect(() => {
    if (selectedEventId && selectedEventId !== -1) {
      getEvent(selectedEventId);
    }
  }, [selectedEventId, getEvent]);

  useEffect(() => {
    if (chosenEvent) {
      setMapState({
        center: [chosenEvent.location.xcords, chosenEvent.location.ycords],
        zoom: 20,
      });
      setEventPanelVisible(true);
    }
  }, [chosenEvent]);

  const onPlacemarkClick = (eventId) => {
    if (eventId === visibleEventId) {
      setEventPanelVisible(false);
      setVisibleEventId(-1);
      return;
    }
    setVisibleEventId(eventId);
    getEvent(eventId);
  };

  const placemarks = events.map((e) => (
    // eslint-disable-next-line react/jsx-key
    <Placemark
      geometry={[e.location.xcords, e.location.ycords]}
      onClick={() => onPlacemarkClick(e.eventId)}
      key={e.eventId}
    />
  ));

  const defaultState = { center: [56.322972, 43.997492], zoom: 15 };

  return (
    <>
      <Map
        defaultState={defaultState}
        state={mapState !== null ? mapState : defaultState}
        width={1500}
        height={750}
      >
        {placemarks}
      </Map>
      {eventPanelVisible ? (
        <EventPanel
          event={chosenEvent}
          subscribe={subscribe}
          unsubscribe={unsubscribe}
          isSubscribed={
            subscribedEvents.find((e) => e === chosenEvent.id) !== undefined
          }
        />
      ) : (
        <></>
      )}
    </>
  );
};

export default MainMap;
