import { combineReducers } from "redux";
import eventPageReducer from "./event-page/reducers/event-page-reducer";
import eventListReducer from "./events-list/reducers/event-list-reducer";
import loginPageReducer from "./login-page/reducers/login-page-reducer";
import registrationPageReducer from "./registration-page/reducers/registration-page-reducer";
import CommentsBlockReducer from "./comments-block/reducers/comments-block-reducer";
import UserPageReducer from "./user-page/reducers/user-page-reducer";
import mainMapReducer from "./main-map/reducers/main-map-reducer";
import headerReducer from "./header/reducers/header-reducer";
import editEventReducer from "./edit-event-window/reducers/edit-event-reducer";
import AddEventWindowReducer from "./add-event-window/reducers/add-event-window-reducer";
import editUserReducer from "./edit-user-window/reducers/edit-user-window-reducer";

const rootReducer = combineReducers({
  eventList: eventListReducer,
  loginPage: loginPageReducer,
  registrationPage: registrationPageReducer,
  eventPage: eventPageReducer,
  commentsBlock: CommentsBlockReducer,
  userPage: UserPageReducer,
  mainMap: mainMapReducer,
  header: headerReducer,
  editEventWindow: editEventReducer,
  addEventWindow: AddEventWindowReducer,
  editUserWindow: editUserReducer,
});

export default rootReducer;
