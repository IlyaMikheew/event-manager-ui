import { Stomp } from "@stomp/stompjs";
import SockJS from "sockjs-client";
import { notification } from "antd";
import Cookies from "universal-cookie/lib";

let stompClient = null;

export function connect() {
  const cookies = new Cookies();
  const socket = new SockJS("http://localhost:8080/ws");
  stompClient = Stomp.over(socket);
  stompClient.connect({}, (frame) => {
    console.log(`Connected: ${frame}`);
    stompClient.subscribe(
      `/topic/notifications/${cookies.get("userId")}`,
      (message) => {
        console.log(message.body);
        notification.open({
          message: "One of your events has been changed",
          description: message.body,
          duration: 7,
        });
      }
    );
  });
}

export function disconnect() {
  if (stompClient !== null) {
    stompClient.disconnect();
  }
  console.log("Disconnected");
}
