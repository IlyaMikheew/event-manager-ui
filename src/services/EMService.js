import axios from "axios";
import Cookies from "universal-cookie";

const cookies = new Cookies();

export default class EMService {
  // eslint-disable-next-line class-methods-use-this
  async getAllUsers() {
    const response = await axios
      .get("http://localhost:8080/users")
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async getEvents(values) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios.post("http://localhost:8080/events", values, {
      params: {
        userEmail: cookies.get("email"),
      },
      headers: authorization,
    });
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async getUser({ userId, pagination }) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .post(
        `http://localhost:8080/users/${userId}`,
        { ...pagination },
        {
          headers: authorization,
        }
      )
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async createUser(userData) {
    const user = {
      ...userData,
      eventsCreated: 0,
      regDate: "2021-05-24T19:10:05",
    };
    const response = await axios
      .post("http://localhost:8080/registration", user)
      .catch((e) => e.response);
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async login(user) {
    const response = await axios
      .post("http://localhost:8080/login", user)
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async getEvent(id) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    // eslint-disable-next-line no-unused-vars
    const response = await axios.get(`http://localhost:8080/events/${id}`, {
      headers: authorization,
    });
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async getComments({ eventId, pagination }) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .get("http://localhost:8080/comments", {
        params: {
          eventId,
          start: pagination.start,
          limit: pagination.limit,
          page: pagination.page,
          userEmail: cookies.get("email"),
        },
        headers: authorization,
      })
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async setComment(comment) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .post("http://localhost:8080/comments", comment, {
        headers: authorization,
      })
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async setLikeOnComment({ commentId, page }) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .post(
        "http://localhost:8080/comments/likes",
        {},
        {
          params: {
            commentId,
            userEmail: cookies.get("email"),
            page,
          },
          headers: authorization,
        }
      )
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async deleteLikeOnComment({ commentId, page }) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .delete("http://localhost:8080/comments/likes", {
        params: {
          commentId,
          userEmail: cookies.get("email"),
          page,
        },
        headers: authorization,
      })
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async subscribe(eventId) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .post(
        "http://localhost:8080/events/subscribe",
        {},
        {
          params: {
            eventId,
            userEmail: cookies.get("email"),
          },
          headers: authorization,
        }
      )
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async unsubscribe(eventId) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .delete("http://localhost:8080/events/subscribe", {
        params: {
          eventId,
          userEmail: cookies.get("email"),
        },
        headers: authorization,
      })
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async getAllEvents() {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .get("http://localhost:8080/events", {
        params: {
          userEmail: cookies.get("email"),
        },
        headers: authorization,
      })
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async searchEvent(namePart) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .get("http://localhost:8080/events/search", {
        params: { namePart },
        headers: authorization,
      })
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async editEvent(event) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .post(
        "http://localhost:8080/events/edit",
        {
          ...event,
        },
        {
          params: {
            userEmail: cookies.get("email"),
          },
          headers: authorization,
        }
      )
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async createEvent(event) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .post("http://localhost:8080/events/create", event, {
        params: {
          userEmail: cookies.get("email"),
        },
        headers: authorization,
      })
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }

  // eslint-disable-next-line class-methods-use-this
  async editUser(user) {
    const authorization = cookies.get("token")
      ? { Authorization: `Bearer_${cookies.get("token")}` }
      : {};
    const response = await axios
      .post(
        "http://localhost:8080/users/edit",
        {
          ...user,
        },
        {
          params: {
            userEmail: cookies.get("email"),
          },
          headers: authorization,
        }
      )
      .catch((e) => console.log(`Request failed: ${e}`));
    return response;
  }
}
